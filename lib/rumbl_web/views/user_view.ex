defmodule RumblWeb.UserView do
  use RumblWeb, :view

  alias Rumbl.Accounts

  def first_name(%Accounts.User{name: name}) do
    name
    |> String.split(" ")
    |> Enum.at(0)
  end

  def last_name(%Accounts.User{name: name}) do
    split_name = name |> String.split(" ")
    split_name |> Enum.at(Enum.count(split_name) - 1)
  end
end